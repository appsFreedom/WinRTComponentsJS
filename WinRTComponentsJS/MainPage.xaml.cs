﻿using MyRuntimeComponent;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Web.Http;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace WinRTComponentsJS
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            // Download file as byte array.
            string uri = "https://qa.mfgfreedom.com/ui/runtime.zip";
            HttpClient httpClient = new HttpClient();
            var result = await httpClient.GetAsync(new Uri(uri));
            var buffer = await result.Content.ReadAsBufferAsync();

            // Save zip archive as storage file.
            StorageFile zipFile = await ApplicationData.Current.LocalFolder.CreateFileAsync("appsFreedom.zip", CreationCollisionOption.ReplaceExisting);
            using (Stream zipStreamWrite = await zipFile.OpenStreamForWriteAsync())
            {
                byte[] bytes = new byte[buffer.Length];
                buffer.CopyTo(bytes);
                await zipStreamWrite.WriteAsync(bytes, 0, bytes.Length);
            }

            // Unzip archive.
            using (Stream zipStreamRead = await zipFile.OpenStreamForReadAsync())
            {
                using (ZipArchive zipArchive = new ZipArchive(zipStreamRead, ZipArchiveMode.Read))
                {
                    // Check if folder already exists.
                    try
                    {
                        var folder = await ApplicationData.Current.LocalFolder.GetFolderAsync("appsFreedom");
                        await folder.DeleteAsync();
                    }
                    catch
                    {
                        /* folder does not exist, do nothing */
                    }

                    string unzipPath = Path.Combine(ApplicationData.Current.LocalFolder.Path, "appsFreedom");
                    zipArchive.ExtractToDirectory(unzipPath);
                }
            }

            webView.NavigationStarting += (s, args) =>
            {
                // Create WinRT component and pass it into webview.
                webView.AddWebAllowedObject("winrtObject", new CordovaSample());
            };
            // Load index.html into webview (assume that we know the path).
            webView.Navigate(new Uri("ms-appdata:///Local/appsFreedom/runtime/index.html"));
        }
    }
}
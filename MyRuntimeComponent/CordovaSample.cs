﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation.Metadata;

namespace MyRuntimeComponent
{
    [AllowForWeb]
    public sealed class CordovaSample
    {
        private readonly string _guid;

        public CordovaSample()
        {
            _guid = Guid.NewGuid().ToString();
        }

        public string Copy()
        {
            return _guid;
        }
    }
}